<?php

/**
 * @file
 * Drush integration for entityqueue_generate module.
 */

/**
 * Implements hook_drush_command().
 */
function entityqueue_generate_drush_command() {
  $items = array();

  // Command to populate given queue(s) with random content.
  $items['entityqueue-generate'] = array(
    'description' => 'Populates given queue with random content.',
    'drupal dependencies' => array('entityqueue_generate'),
    'arguments' => array(
      'entityqueue' => 'Machine name of entityqueue.',
    ),
    'aliases' => array('eqg'),
  );

  // Command to populate ALL queues with random content.
  $items['entityqueue-generate-all'] = array(
    'description' => 'Populates given queue with random content.',
    'drupal dependencies' => array('entityqueue_generate'),
    'aliases' => array('eqga'),
  );

  // Command to truncate table with mostviewed data.
  $items['entityqueue-generate-empty'] = array(
    'description' => 'Empties specified entityqueue.',
    'drupal dependencies' => array('entityqueue_generate'),
    'arguments' => array(
      'entityqueue' => 'Machine name of entityqueue.',
    ),
    'aliases' => array('eqe'),
  );
  // Command to truncate table with mostviewed data.
  $items['entityqueue-generate-empty-all'] = array(
    'description' => 'Empties all entityqueues.',
    'drupal dependencies' => array('entityqueue_generate'),
    'aliases' => array('eqea'),
  );
  $items['entityqueue-generate-list'] = array(
    'description' => 'List all available entityqueues.',
    'drupal dependencies' => array('entityqueue_generate'),
    'aliases' => array('eql'),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function entityqueue_generate_drush_help($section) {
  switch ($section) {

    case 'drush:entityqueue-generate':
      return dt('Generates dummy data for specified entityqueue.');

    case 'drush:entityqueue-generate-all':
      return dt('Generates dummy data for all entityqueues.');

    case 'drush:entityqueue-generate-empty':
      return dt('Empties specified entityqueue.');

    case 'drush:entityqueue-generate-empty-all':
      return dt('Empties all entityqueues.');

    case 'drush:entityqueue-generate-list':
      return dt('List all available entityqueues.');

  }
}

/**
 * Populates given queue with content.
 */
function drush_entityqueue_generate() {
  $queue_names = func_get_args();
  if (empty($queue_names)) {
    return drush_set_error('ENTITYQUEUE_GENERATE_UNSPECIFIED_ENTITYQUEUE', dt('You must specify entityqueue machine name(s)'));
  }
  if (drush_confirm(dt('Do you really want to fill specified entityqueues out (queues: !queues)?', array('!queues' => implode(',', $queue_names))))) {
    drush_print('Generate specific');
    $queues = entityqueue_queue_load_multiple($queue_names);
    foreach ($queues as $queue) {
      entityqueue_generate__populate_queue($queue);
      drush_print($queue->name);
    }
  }
}

/**
 * Populates all queue with content.
 */
function drush_entityqueue_generate_all() {
  if (drush_confirm(dt('Do you really want to fill ALL entityqueues out?'))) {
    drush_print('Generate all!');
    $queues = entityqueue_queue_load_multiple();
    ksort($queues);
    foreach ($queues as $queue) {
      entityqueue_generate__populate_queue($queue);
      drush_print($queue->name);
    }
  }
}

/**
 * Empties given queue.
 */
function drush_entityqueue_generate_empty() {
  $queue_names = func_get_args();
  if (empty($queue_names)) {
    return drush_set_error('ENTITYQUEUE_GENERATE_UNSPECIFIED_ENTITYQUEUE', dt('You must specify entityqueue machine name(s)'));
  }
  if (drush_confirm(dt('Do you really want to empty specified entityqueues (queues: !queues)?', array('!queues' => implode(',', $queue_names))))) {
    $queues = entityqueue_queue_load_multiple($queue_names);
    foreach ($queues as $queue) {
      entityqueue_generate__empty_queue($queue);
      drush_print($queue->name);
    }
    drush_print('Empty specified');
  }
}

/**
 * Empties all queues.
 */
function drush_entityqueue_generate_empty_all() {
  if (drush_confirm(dt('Do you really want to empty all entityqueues?'))) {
    drush_print('Empty ALL!');
    $queues = entityqueue_queue_load_multiple();
    ksort($queues);
    foreach ($queues as $queue) {
      entityqueue_generate__empty_queue($queue);
      drush_print($queue->name);
    }
  }
}

/**
 * Lists all avaialable queues.
 */
function drush_entityqueue_generate_list() {
  $queues = entityqueue_queue_load_multiple();
  ksort($queues);
  foreach ($queues as $queue) {
    drush_print($queue->label . ' : ' . $queue->name);
  }
}
